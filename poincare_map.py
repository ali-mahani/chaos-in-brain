#!/usr/bin/env python

""" Plotting the poincare section map """

from model import Model
from phase_portrait import np, plt, rk4


def main():
    """ main body """
    # defining the model
    param = {
        'a_1': 3,
        'a_2': 9.8,
        'b_1': 2.2,
        'b_2': 2.2,
        'c_1': 2,
        'd': 10,
        'gamma_1': 0.1,
        'gamma_2': 0.1,
        'omega_1': 2 * np.pi,
        'omega_2': 2 * np.pi,
        'epsilon_1': 17.09,
        'epsilon_2': 2.99,
    }
    model = Model(**param, Omega=15)

    X_0 = [0.1, 0.6, 0.1, 0.8]
    dots = [model.x_dot, model.u_dot, model.y_dot, model.v_dot]
    step = 0.005
    end = 20

    time, data = rk4(X_0, dots, step, end)

    index = np.where(data[:, 2] - 5 < 0.00001)[0]
    print(index)

    plt.plot(data[index, 0], data[index, 1], ls = '', marker='+')
    plt.xlabel('x')
    plt.ylabel('u')
    plt.grid()
    plt.title('poincare map made by crossing (x, u, y) in y = 5')
    plt.savefig('results/poincare_map.jpg', dpi=200, bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
    main()
