import numpy as np


class Model:
    def __init__(self, a_1, a_2, b_1, b_2, c_1, d, gamma_1, gamma_2, omega_1, omega_2,
                 epsilon_1, epsilon_2, Omega):
        self.a_1 = a_1
        self.a_2 = a_2
        self.b_1 = b_1
        self.b_2 = b_2
        self.c_1 = c_1
        self.d = d
        self.gamma_1 = gamma_1
        self.gamma_2 = gamma_2
        self.omega_1 = omega_1
        self.omega_2 = omega_2
        self.epsilon_1 = epsilon_1
        self.epsilon_2 = epsilon_2
        self.Omega = Omega

        self.param = [a_1, a_2, b_1, b_2, c_1, d, gamma_1, gamma_2, omega_1, omega_2,
                      epsilon_1, epsilon_2, Omega]

    def x_dot(self, t, X):
        return X[1]

    def u_dot(self, t, X):
        return -((self.gamma_1 + self.a_1 * (X[0] ** 2) + self.b_1 * (X[2] ** 2)) * X[1] +
                 (self.omega_1 ** 2) * (1 + self.epsilon_1 * np.sin(2 * self.Omega * t)) * X[0] +
                 self.c_1 * (X[2] ** 2) * np.sin(self.Omega * t))

    def y_dot(self, t, X):
        return X[3]

    def v_dot(self, t, X):
        return -((self.gamma_2 + self.a_2 * (X[2] ** 2) + self.b_2 * (X[0] ** 2)) * X[3] +
                 (self.omega_2 ** 2) * (1 + self.epsilon_2 * np.sin(2 * self.Omega * t)) * X[2] +
                 self.d * X[1])

    def jacobian(self, t, X):
        return np.array([
            [0, 1, 0, 0],
            [-(2 * self.a_1 * X[0] * X[1] + (self.omega_1 ** 2) * (1 + self.epsilon_1 * np.sin(2 * self.Omega * t))),
             -(self.gamma_1 + self.a_1 * (X[0] ** 2) + self.b_1 * (X[2] ** 2)),
             -(2 * self.b_1 * X[2] * X[1] + 2 * self.c_1 * X[2] * np.sin(self.Omega * t)),
             0],
            [0, 0, 0, 1],
            [-(2 * self.b_2 * X[0] * X[3]),
             -self.d,
             -(2 * self.a_2 * X[2] * X[3] + (self.omega_2 ** 2) * (1 + self.epsilon_2 * np.sin(2 * self.Omega * t))),
             -(self.gamma_2 + self.a_2 * (X[2] ** 2) + self.b_2 * (X[0] ** 2))],
        ])
