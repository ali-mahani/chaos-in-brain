#!/usr/bin/env python

""" Here we draw the 2-D phase portrait of the equations """

import numpy as np
import matplotlib.pyplot as plt
from runge_kutta4 import rk4
from model import Model


def main():
    """ main body """
    # define the constants of the model
    param = {
        'a_1': 3,
        'a_2': 9.8,
        'b_1': 2.2,
        'b_2': 2.2,
        'c_1': 2,
        'd': 10,
        'gamma_1': 0.1,
        'gamma_2': 0.1,
        'omega_1': 2 * np.pi,
        'omega_2': 2 * np.pi,
        'epsilon_1': 17.09,
        'epsilon_2': 2.99,
    }

    # defining the model
    model = Model(**param, Omega=15)

    X_0 = [0.1, 0.6, 0.1, 0.8]

    dots = [model.x_dot, model.u_dot, model.y_dot, model.v_dot]
    step = 0.005
    end = 50

    time, data = rk4(X_0, dots, step, end)

    # plot y(t)
    fig, ax = plt.subplots(1, 1, figsize=(20, 8))
    ax.plot(time[:5000], data[:5000, 2], lw=1, label='y(t)')
    ax.set_xlabel('t')
    ax.set_ylabel('y')
    plt.legend()
    plt.savefig('results/yplot.jpg', dpi=200, bbox_inches='tight')
    plt.show()

    # plot phase portrait for (x, u)
    plt.plot(data[:, 0], data[:, 1], lw=1, label='u(x)')
    plt.xlabel('x')
    plt.ylabel('u')
    plt.legend()
    plt.title('phase portrait u(x)')
    plt.savefig('results/portrait_xu.jpg', dpi=200, bbox_inches='tight')
    plt.show()

    # plot phase portrait for (y, v)
    plt.plot(data[:, 2], data[:, 3], lw=1, label='v(y)')
    plt.xlabel('y')
    plt.ylabel('v')
    plt.legend()
    plt.title('phase portrait v(y)')
    plt.savefig('results/portrait_yv.jpg', dpi=200, bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
    main()
